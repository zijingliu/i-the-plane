﻿using UnityEngine;
using System.Collections;

public class Plane_Model_Controller : MonoBehaviour {

	public float FlyingSpeed;
	// Use this for initialization
	void Start () 
	{
		Vector2 newVelocity = GetComponent<Rigidbody2D> ().velocity;
		newVelocity.x = FlyingSpeed;
		GetComponent<Rigidbody2D> ().velocity = newVelocity;
	}
	
	void OnTriggerEnter2D(Collider2D collider)
	{
		Destroy (gameObject);
	}
}
