﻿using UnityEngine;
using System.Collections;

public class LaserController : MonoBehaviour 
{
	
	public Sprite OnSprite;    
	public Sprite OffSprite; 
	public float MoveX;
	public float MoveY;
	public float RangeX;
	public float RangeY;
	public float interval = 0.5f;    
	public float rotationSpeed = 0.0f;
	private bool isLaserOn = true;    
	private float timeUntilNextToggle;
	private Vector3 originalposition;

	// Use this for initialization
	void Start () 
	{
		timeUntilNextToggle = interval;
		originalposition = transform.position;
	}
	
	void FixedUpdate () 
	{
		timeUntilNextToggle -= Time.fixedDeltaTime;

		if (timeUntilNextToggle <= 0) 
		{
			isLaserOn = !isLaserOn;
			GetComponent<Collider2D>().enabled = isLaserOn;

			SpriteRenderer spriteRenderer = ((SpriteRenderer)this.GetComponent<Renderer>());
			if (isLaserOn)
			{
				spriteRenderer.sprite = OnSprite;
			}
			else
			{
				spriteRenderer.sprite = OffSprite;
			}
			timeUntilNextToggle = interval;
		}

		transform.RotateAround(transform.position, Vector3.forward, rotationSpeed * Time. fixedDeltaTime);
		if (Mathf.Abs (transform.position.x - originalposition.x) > RangeX) 
		{
			//transform.position = new Vector3 (transform.position.x - MoveX, transform.position.y, transform.position.z);
			MoveX = 0.0f - MoveX;
		}
		else if(Mathf.Abs (transform.position.y - originalposition.y) > RangeY)
		{
			//transform.position = new Vector3 (transform.position.x, transform.position.y - MoveX, transform.position.z);
			MoveY = 0.0f - MoveY;
		}

		transform.position = new Vector3 (transform.position.x + MoveX, transform.position.y + MoveY, transform.position.z);
	}
}