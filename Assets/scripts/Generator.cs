﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 

public class Generator : MonoBehaviour {
 
	public GameObject[] allRooms;
	public List<GameObject> currentRooms;

	public GameObject[] availableObjects_1; 
	public GameObject[] availableObjects_2; 
	public List<GameObject> currentObjects;
	
	public float objectsMinDistance = 5.0f;    
	public float objectsMaxDistance = 10.0f;
	
	public float objectsMinY = -1.4f;
	public float objectsMaxY = 1.4f;
	
	public float objectsMinRotation = -45.0f;
	public float objectsMaxRotation = 45.0f;
	
	private float screenWidthInPoints; 
	private float threshold = 0;
	private bool updateLevel = false;

	// Use this for initialization
	void Start () 
	{
		float height = 2.0f * Camera.main.orthographicSize;
		screenWidthInPoints = height * Camera.main.aspect; 
	
	}
	
	// Update is called once per frame
	void FixedUpdate () 
	{
		threshold = gameObject.GetComponent<PlayerController> ().scores;
		GenerateRoomIfRequired();
		GenerateObjectsIfRequired ();
	}

	void AddRoom(float RoomEndX)
	{
		int randomRoomIndex = Random.Range(0, allRooms.Length);	
		GameObject room = (GameObject)Instantiate(allRooms[randomRoomIndex]);

		float roomWidth = room.transform.FindChild("Floor").localScale.x;

		float roomCenter = RoomEndX + roomWidth * 0.5f;

		room.transform.position = new Vector3(roomCenter, 0, 0);
		currentRooms.Add(room);         
	}

	void AddObject(float lastObjectX)
	{
		GameObject obj;
		if(threshold > 20)
		{
			if(!updateLevel)
			{
				updateLevel = true;
				objectsMinDistance += 3.5f;
				objectsMaxDistance += 2.5f;
				objectsMaxY -= 2.0f;
				objectsMinY += 1.0f;
			}
			int randomIndex = Random.Range(0, availableObjects_2.Length);
			obj = (GameObject)Instantiate(availableObjects_2[randomIndex]);
		}
		else
		{
			int randomIndex = Random.Range(0, availableObjects_1.Length);
			obj = (GameObject)Instantiate(availableObjects_1[randomIndex]);
		}

		float objectPositionX = lastObjectX + Random.Range(objectsMinDistance, objectsMaxDistance);
		float randomY = Random.Range(objectsMinY, objectsMaxY);
		obj.transform.position = new Vector3(objectPositionX,randomY,0);

		float rotation = Random.Range(objectsMinRotation, objectsMaxRotation);
		obj.transform.rotation = Quaternion.Euler(Vector3.forward * rotation);

		currentObjects.Add(obj);            
	}

	void GenerateRoomIfRequired()
	{
		List<GameObject> roomsToRemove = new List<GameObject>();
		bool addRooms = true;        
		float playerX = transform.position.x;
		float removeRoomX = playerX - screenWidthInPoints;        
		float addRoomX = playerX + screenWidthInPoints;
		float RoomEndX = 0;
		
		foreach(var room in currentRooms)
		{
			float roomWidth = room.transform.FindChild("Floor").localScale.x;
			float roomStartX = room.transform.position.x - (roomWidth * 0.5f);    
			float roomEndX = roomStartX + roomWidth;                            

			if (roomStartX > addRoomX)
			{
				addRooms = false;
			}

			if (roomEndX < removeRoomX)
			{
				roomsToRemove.Add(room);
			}
	
			RoomEndX = Mathf.Max(RoomEndX, roomEndX);
		}

		foreach(var room in roomsToRemove)
		{
			currentRooms.Remove(room);
			Destroy(room);            
		}

		if (addRooms)
		{
			AddRoom(RoomEndX);
		}
	}

	void GenerateObjectsIfRequired()
	{
		float playerX = transform.position.x;        
		float removeObjectsX = playerX - screenWidthInPoints;
		float addObjectX = playerX + screenWidthInPoints;
		float farthestObjectX = 0;
		
		//2
		List<GameObject> objectsToRemove = new List<GameObject>();
		
		foreach (var obj in currentObjects)
		{
			if(obj != null)
			{
				float objX = obj.transform.position.x;
				farthestObjectX = Mathf.Max(farthestObjectX, objX);

				if (objX < removeObjectsX)
				{
					objectsToRemove.Add(obj);
				}
			}
		}

		foreach (var obj in objectsToRemove)
		{
			currentObjects.Remove(obj);
			Destroy(obj);
		}
		
		//7
		if (farthestObjectX < addObjectX)
		{
			AddObject(farthestObjectX);
		}
	}
}
