﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

	public Text scoreNumber;
	public Text feulPercentage;
	public GUIStyle restartButtonStyle;
	public ParticleSystem FirePartical;

	public float playerForce = 75.0f;
	public float FlyingSpeed = 3.0f;
	//public float frequence;
	public float deduction;
	public float Boostdeduction;
	public float increasement;

	public AudioClip coinCollectSound;
	public AudioClip explodeSound;

	public int scores = 0;

	private bool dead;
	private Animator animator;
	private bool onceDead = false;
	private float originalLength;
	//private float nextcheck = 0;

	void Start()
	{
		dead = false;
		animator = GetComponent<Animator>();
		originalLength = FirePartical.startLifetime;
		Vector2 newVelocity = GetComponent<Rigidbody2D> ().velocity;
		newVelocity.x = FlyingSpeed;
		GetComponent<Rigidbody2D> ().velocity = newVelocity;
		//GetComponent<Rigidbody2D>().AddForce(new Vector2(5f, 0f));
		//GetComponent<Rigidbody2D>().AddForce(new Vector2(-5f, 0f));
		//nextcheck += frequence;
	}

	/*void Update()
	{
		if (Time.time > nextcheck)
		{
			nextcheck = Time.time + frequence;
			playerForce -= deduction;
			feulPercentage.text = ((int)((playerForce / 75.0f) * 100)).ToString () + "%";
		}
	}*/

	void FixedUpdate () 
	{

		bool playerActive = Input.GetButton("Jump");

		AdjustFire(playerActive, dead); 
		
		if (playerActive && !dead)
		{
			if(playerForce > 0)
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2(0, playerForce));
				playerForce -= deduction;
				feulPercentage.text = ((int)((playerForce / 75.0f) * 100)).ToString () + "%";
			}
		}

		if (!dead) 
		{
			if(GetComponent<Rigidbody2D> ().velocity.x > FlyingSpeed)
			{
				GetComponent<Rigidbody2D>().AddForce(new Vector2(-50f, 0f));
			}
			else
			{
				Vector2 newVelocity = GetComponent<Rigidbody2D> ().velocity;
				newVelocity.x = FlyingSpeed;
				GetComponent<Rigidbody2D> ().velocity = newVelocity;
			}
		}

		if(dead && !onceDead)
		{
			GetComponent<Rigidbody2D> ().isKinematic = true;
			AudioSource.PlayClipAtPoint(explodeSound, transform.position, 0.5f);
			onceDead = true;
		}

		if(Input.GetKey(KeyCode.B) && !dead)
		{
			GetComponent<Rigidbody2D>().AddForce(new Vector2(100f, 0f));
			FirePartical.startLifetime = 2.0f * originalLength;
			FirePartical.enableEmission = true;
			playerForce -= Boostdeduction;
			feulPercentage.text = ((int)((playerForce / 75.0f) * 100)).ToString () + "%";
		}
	}

	void OnGUI()
	{
		DisplayRestartButton ();
	}

	void AdjustFire (bool playerActive, bool dead)
	{
		FirePartical.enableEmission = playerActive && !dead;
		if(!dead)
		{
			FirePartical.emissionRate = (3.0f * playerForce);//playerActive ? (3.0f * playerForce) : (1.0f * playerForce); 
			FirePartical.startLifetime = originalLength;
		}
	} 

	void OnTriggerEnter2D(Collider2D collider)
	{
		if(collider.tag == "Coin")
		{
			CollectCoid(collider);
			return;
		}
		else
		{
			HitByLaser(collider);
		}
	}
	
	void HitByLaser(Collider2D laserCollider)
	{
		dead = true;
		animator.SetBool("Dead", true);
	}

	void CollectCoid(Collider2D collider)
	{
		scores++;
		Destroy (collider.gameObject);
		scoreNumber.text = scores.ToString();
		AudioSource.PlayClipAtPoint(coinCollectSound, transform.position, 0.35f);
		FlyingSpeed += 0.1f;
		if(playerForce < (75.0f - increasement))
		{
			playerForce += increasement;
		}
		else if(playerForce < 75.0f)
		{
			playerForce = 75.0f;
		}
		feulPercentage.text = ((int)((playerForce / 75.0f) * 100)).ToString () + "%";
	}

	void DisplayRestartButton()
	{
		if (dead)
		{
			Rect buttonRect = new Rect(Screen.width * 0.35f, Screen.height * 0.45f, Screen.width * 0.30f, Screen.height * 0.1f);
			if (GUI.Button(buttonRect, "Tap to restart!", restartButtonStyle))
			{
				Application.LoadLevel (Application.loadedLevelName);
			}
		}
	}
}







