﻿using UnityEngine;
using System.Collections;

public class ParticalSystemLayerSorting : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingLayerName = "Player";
		GetComponent<ParticleSystem>().GetComponent<Renderer>().sortingOrder = -1;
	}
}
