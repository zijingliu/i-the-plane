﻿using UnityEngine;
using System.Collections;

public class StartGame : MonoBehaviour 
{
	public GameObject plane;
	public GameObject spawnLocation;

	public void ChangeScence(string scenceName)
	{
		Application.LoadLevel(scenceName);
	}

	void FixedUpdate()
	{
		if(!GameObject.FindGameObjectWithTag("Plane_Model"))
		{
			Instantiate(plane, spawnLocation.transform.position, Quaternion.identity);
		}
	}
}
