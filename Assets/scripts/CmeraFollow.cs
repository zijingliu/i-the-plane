﻿using UnityEngine;
using System.Collections;

public class CmeraFollow : MonoBehaviour {

	public GameObject targetObject;

	private float distance;

	void Start()
	{
		distance = transform.position.x - targetObject.transform.position.x;
	}

	void FixedUpdate () 
	{
		float targetObjectX = targetObject.transform.position.x;
		Vector3 newCameraPosition = transform.position;
		newCameraPosition.x = targetObjectX + distance;
		transform.position = newCameraPosition;
	}
}
